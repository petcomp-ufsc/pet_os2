#!/bin/zsh

######### trizen ########
cd /home/pet/; wget https://aur.archlinux.org/cgit/aur.git/snapshot/trizen.tar.gz

tar xvf /home/pet/trizen.tar.gz
rm /home/pet/*.tar.gz

cd /home/pet/trizen; makepkg; sudo pacman -U *.pkg.tar.xz --noconfirm
rm -r /home/pet/trizen/
#########################

######### aur ###########
while read aur; do 
  trizen -S $aur --noconfirm
done < /home/pet/packages.aur
#########################

date -R > ~/.build
