#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(pt_BR\.*\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root

sed -i "1s/^/Server = http:\/\/pet\.inf\.ufsc\.br\/mirrors\/archlinux\/\$repo\/os\/\$arch\n/" /etc/pacman.d/mirrorlist
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

systemctl enable pacman-init.service choose-mirror.service
systemctl set-default graphical.target

rm etc/systemd/system/getty@tty1.service.d/autologin.conf

sed -i "s/#\(auto_login \).\+/\1yes/" /etc/slim.conf
sed -i "s/#\(default_user \).\+/\1pet/" /etc/slim.conf

systemctl enable slim
systemctl enable dhcpcd
systemctl enable NetworkManager

echo '[multilib]'                         >> /etc/pacman.conf
echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf
sed -i 's/\(CheckSpace\)/#\1/' /etc/pacman.conf
pacman-key --init
pacman-key --populate archlinux
pacman -Syu --noconfirm

cp /root/wallpaper.png /usr/share/backgrounds/gnome/adwaita-day.jpg 
cp /root/wallpaper.png /usr/share/backgrounds/gnome/adwaita-morning.jpg 
mv /root/wallpaper.png /usr/share/backgrounds/gnome/adwaita-night.jpg 

pip install poetry

useradd -m -G lock,uucp -s /usr/bin/zsh pet

chown -R pet:pet /home/pet/
chmod +x /home/pet/Desktop/*

sed -i "s/# \(ALL ALL=(ALL) \).\+/\1NOPASSWD: ALL/" /etc/sudoers

su -c "/home/pet/customize_ainorootfs.sh" -l pet

sed -i "s/\(ALL ALL=(ALL).\+\)/\# \1/" /etc/sudoers
rm -f /home/pet/customize_ainorootfs.sh
rm -f /home/pet/packages.aur

passwd -l root

